import React from 'react'
import { Route, IndexRoute } from 'react-router'
import App from '../client/App/App'
import Home from '../client/Home/Home'

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
  </Route>
)
