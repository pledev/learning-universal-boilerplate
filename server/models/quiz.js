import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const quizSchema = new Schema({
  question: { type: 'String', required: true },
  choices: [{
    num: { type: 'Number', required: true},
    answer: { type: 'String', required: true }
  }],
  trueAnswer: [{ type: 'Number', required: true }],
  totalChoices: { type: 'Number', required: true },
  explain: { type: 'String' }
});

export default mongoose.model('Quiz', quizSchema);
