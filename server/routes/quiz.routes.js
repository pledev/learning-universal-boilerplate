import { Router } from 'express';
import * as QuizController from '../controllers/quiz.controller';
const router = new Router();

router.route('/quizs')
  .get(QuizController.getQuizs)
  .post(QuizController.addQuiz);

export default router;
  
