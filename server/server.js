import Express from 'express'
import mongoose from 'mongoose'
import helmet from 'helmet'
import compression from 'compression'
import bodyParser from 'body-parser'
import path from 'path'

import webpack from 'webpack'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'
import webpackConfig from '../webpack.config.dev'

import React from 'react'
import { Provider } from 'react-redux'
import { renderToString } from 'react-dom/server'
import { match, RouterContext } from 'react-router'
import Helmet from 'react-helmet'

import quizRouter from './routes/quiz.routes'
import serverConfig from './config/config'
import { fetchComponentData } from './util/DataFetch'
import configureStore from '../common/store'
import routes from '../common/routes'

const app = new Express()

mongoose.connect(serverConfig.mongoURL, (error) => {
  if (error) {
    console.error('Please make sure Mongodb is installed and running!'); // eslint-disable-line no-console
    throw error;
  }
});

if(process.env.NODE_ENV === 'development') {
  const compiler = webpack(webpackConfig)
  app.use(webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: webpackConfig.output.publicPath
  }))
  app.use(webpackHotMiddleware(compiler))
}

app.use(compression())
  .use(helmet())
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: false }))
  .use(Express.static(path.resolve(__dirname, '../dist')))
  .use('/api', quizRouter)

app.use((req, res, next) => {
  global.navigator = global.navigator || {};
  global.navigator.userAgent = req.headers['user-agent'] || 'all';

  match({ routes, location: req.url }, (err, redirectLocation, renderProps) => {
    if (redirectLocation) {
      return res.redirect(302, redirectLocation.pathname + redirectLocation.search);
    } else if (err) {
      return res.status(500).send(err.message)
    } else if (!renderProps) {
      return res.status(404).send('Not Found')
    }

    const store = configureStore();

    return fetchComponentData(store, renderProps.components, renderProps.params)
      .then(() => {
        const initialView = renderToString(
          <Provider store={store}>
            <RouterContext {...renderProps} />
          </Provider>
        );
        const finalState = store.getState();

        res.set('Content-Type', 'text/html')
          .status(200)
          .end(renderFullPage(initialView, finalState));
      }).catch((error) => next(error));
  });
})

const renderFullPage = (html, initialState) => {
  const head = Helmet.rewind()
  return `
  <!doctype html>
  <html>
    <head>
      ${head.base.toString()}
      ${head.title.toString()}
      ${head.meta.toString()}
      ${head.link.toString()}
      ${head.script.toString()}
      <link rel="shortcut icon" href="http://res.cloudinary.com/hashnode/image/upload/v1455629445/static_imgs/mern/mern-favicon-circle-fill.png" type="image/png" />
      <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    </head>
    <body>
      <div id="root">${html}</div>
      <script>
        window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};
      </script>
      <script src='${process.env.NODE_ENV === 'production' ? 'vendor.js' : '/static/vendor.js'}'></script>
      <script src='${process.env.NODE_ENV === 'production' ? 'app.js' : '/static/app.js'}'></script>
    </body>
  </html>
  `
}

app.listen(serverConfig.port, (error) => {
  if (error) {
    console.error(error)
  } else {
    console.info(`🌎 Server is listening on port ${serverConfig.port}`)
  }
})

export default app
