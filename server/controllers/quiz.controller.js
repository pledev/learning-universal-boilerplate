import Quiz from '../models/quiz';

export function getQuizs(req, res) {
  Quiz.find().exec((err, quizs) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ quizs });
  });
}

export function addQuiz(req, res) {
  if(!req.body.quiz.question || !req.body.quiz.choices ||
    !req.body.quiz.trueAnswer || !req.body.quiz.totalChoices) {
    res.status(403).end();
  }

  const newQuiz = new Quiz(req.body.quiz);
  
  newQuiz.save((err, saved) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ quiz: saved });
  });
}
