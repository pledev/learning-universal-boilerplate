import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import HeaderContainer from '../Partials/containers/HeaderContainer'

const App = ({ children }) => {
  return (
    <div>
      <Helmet
        title="Server Side Rendering"
        titleTemplate="%s - React Redux Isomorphism Learning Boilerplate"
        meta={[
          { charset: 'utf-8' },
          { 'http-equiv': 'X-UA-Compatible',content: 'IE=edge' },
          { name: 'viewport', content: 'width=device-width, initial-scale=1' },
          { name: 'description', content: 'Learning React Redux'},
          { name: 'author', content: 'p-le'}
        ]}
      />
      <HeaderContainer />
      {children}
    </div>
  )
}

App.propTypes = {
  children: PropTypes.object
}

export default connect()(App)
